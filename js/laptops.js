const laptopSelect = document.getElementById("laptop-select");
const features = document.getElementById("features");
const image = document.getElementById("image");
const info = document.getElementById("info");
const price = document.getElementById("price");
const buyButton = document.getElementById("buy-button");

fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    .then(computerData => {

        // Populate select
        computerData.forEach(computer => {
            const option = document.createElement("option");
            option.innerHTML = computer.title;
            laptopSelect.appendChild(option);
        });

        // Show features for first computer
        let selectedComputer = computerData[0];
        updateFeaturesAndInfo(selectedComputer);
        
        // Add eventlistner to the laptop select to update features and selected laptop
        laptopSelect.addEventListener("change", (event) => {

            selectedComputer = computerData.find(computer => event.target.value === computer.title);
            
            updateFeaturesAndInfo(selectedComputer);            
        });

        // Add eventlistener to the buy button
        buyButton.addEventListener("click", () => {

            if (selectedComputer.price > bankBalance) {
                alert(`You can not afford ${selectedComputer.title}!`);

            } else {
                changeBank(-selectedComputer.price);
                alert(`You have bought ${selectedComputer.title}, congratulations!`);
                computerBought = true;
            }
        });


    })
    .catch((error) => console.log(error));

function updateFeaturesAndInfo(selectedComputer) {
    // Features
    features.innerHTML = "";

    selectedComputer.specs.forEach(spec => {
        const listItem = document.createElement("li");
        listItem.innerHTML = spec;
        features.appendChild(listItem);
    });

    // Image
    const computerImage = document.createElement("img");
    computerImage.classList.add("computer-image");
    computerImage.src = selectedComputer.image;

    image.innerHTML = "";
    image.appendChild(computerImage);

    // Info
    info.innerHTML = "";
    const infoHeader = document.createElement("h1");
    infoHeader.innerHTML = selectedComputer.title;
    info.appendChild(infoHeader);

    const infoText = document.createElement("p");
    infoText.innerHTML = selectedComputer.description;
    info.appendChild(infoText);

    // Price
    price.innerHTML = "";
    const computerPrice = document.createElement("h2");
    computerPrice.innerHTML = `${selectedComputer.price} Kr`;
    price.appendChild(computerPrice);
}

