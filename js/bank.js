const bankBalanceDisplay = document.getElementById("bank-balance");
const loanBalanceDisplay = document.getElementById("loan-balance");
const loanButton = document.getElementById("loan-button");
const loanDisplay = document.getElementById("loan-display");
const repayLoanButton = document.getElementById("repay-loan-button");

let bankBalance = 300;
let outstandingLoan = 0;
let computerBought = true;
bankBalanceDisplay.innerHTML = bankBalance;

loanButton.addEventListener("click", () => {
    // Can only get a loan if there is no existing loan
    if (outstandingLoan !== 0) {
        alert("Please repay loan before takning another");
        return;
    }

    // Must buy a computer to get another loan
    if (!computerBought) {
        alert("Please buy a computer before takning another loan");
        return;
    }

    const loanAmount = Number(prompt("Please enter amount to loan"));

    const possibleLoan = bankBalance * 2;

    if (loanAmount > possibleLoan) {
        alert(`The loan can not be greater than ${possibleLoan} right now.`);
    } else if (loanAmount > 0) {
        //Successful loan
        if (loanDisplay.classList.contains("hidden")) {
            loanDisplay.classList.remove("hidden");
            repayLoanButton.classList.remove("hidden");
        }

        changeBank(loanAmount);
        changeLoan(loanAmount);

        computerBought = false;

    }
});

function changeBank(moneyAmount) {
    bankBalance += moneyAmount;
    bankBalanceDisplay.innerHTML = bankBalance;
}

function changeLoan(moneyAmount) {
    outstandingLoan += moneyAmount;
    loanBalanceDisplay.innerHTML = outstandingLoan;
}
