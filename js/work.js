const workPayDisplay = document.getElementById("work-pay");
const bankButton = document.getElementById("bank-button");
const workButton = document.getElementById("work-button");

let payBalance = 0;
workPayDisplay.innerHTML = payBalance;

workButton.addEventListener("click", () => {
    changePay(100)
});

bankButton.addEventListener("click", () => {
    if (outstandingLoan > 0) {
        // A loan exists and 10% of the money banked goes to repaying the loan
        const loanDeduction = Math.round(payBalance * 0.1);

        const leftOverFromPayment = moveFromPayToLoan(loanDeduction);
        changeBank(payBalance + leftOverFromPayment);
        changePay(-payBalance);

    } else {
        // Transfer from pay to bank if there is no loan
        changeBank(payBalance);
        changePay(-payBalance);

    }

    checkAndRemoveLoanFeatures();

});

repayLoanButton.addEventListener("click", () => {
    const leftOverFromPayment = moveFromPayToLoan(payBalance);
    changePay(leftOverFromPayment);

    checkAndRemoveLoanFeatures();
})

function changePay(moneyAmount) {
    payBalance += moneyAmount;
    workPayDisplay.innerHTML = payBalance;
}

function moveFromPayToLoan(moneyAmount) {
    if (moneyAmount > outstandingLoan) {
        const difference = moneyAmount - outstandingLoan;
        changeLoan(-(moneyAmount - difference));
        changePay(-moneyAmount);

        return difference;
    } else {
        changePay(-moneyAmount);
        changeLoan(-moneyAmount);

        return 0;
    }
}

function checkAndRemoveLoanFeatures() {
    if (outstandingLoan <= 0) {
        if (!loanDisplay.classList.contains("hidden")) {
            loanDisplay.classList.add("hidden");
            repayLoanButton.classList.add("hidden");
        }
    }
}
